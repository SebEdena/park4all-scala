package MainGraph

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.storage.StorageLevel

object ParisGraph extends Serializable{

  def joinDistrict(spark: SparkSession, nFirstRows: Int): Unit= {
    val dataNearbyDistrict = Seq(("1", "1"), ("1", "2"), ("1", "3"), ("1", "4"), ("1", "6"), ("1", "7"), ("1", "8"), ("1", "9"),
                                ("2", "1"), ("2", "2"), ("2", "3"), ("2", "9"), ("2", "10"),
                                ("3", "1"), ("3", "2"), ("3", "3"), ("3", "4"), ("3", "10"), ("3", "11"),
                                ("4", "1"), ("4", "3"), ("4", "4"), ("4", "5"), ("4", "11"), ("4", "12"),
                                ("5", "4"), ("5", "5"), ("5", "6"), ("5", "12"), ("5", "13"), ("5", "14"),
                                ("6", "1"), ("6", "5"), ("6", "6"), ("6", "7"), ("6", "14"), ("6", "15"),
                                ("7", "1"), ("7", "6"), ("7", "7"), ("7", "8"), ("7", "15"), ("7", "16"),
                                ("8", "1"), ("8", "7"), ("8", "8"), ("8", "9"), ("8", "16"), ("8", "17"),
                                ("9", "1"), ("9", "2"), ("9", "8"), ("9", "9"), ("9", "10"), ("9", "18"),
                                ("10", "2"), ("10", "3"), ("10", "9"), ("10", "10"), ("10", "11"), ("10", "18"), ("10", "19"),
                                ("11", "3"), ("11", "4"), ("11", "10"), ("11", "11"), ("11", "12"), ("11", "20"),
                                ("12", "4"), ("12", "5"), ("12", "11"), ("12", "12"), ("12", "13"), ("12", "20"),
                                ("13", "5"), ("13", "12"), ("13", "13"), ("13", "14"),
                                ("14", "5"), ("14", "6"), ("14", "13"), ("14", "14"), ("14", "15"),
                                ("15", "6"), ("15", "7"), ("15", "14"), ("15", "15"), ("15", "16"),
                                ("16", "7"), ("16", "8"), ("16", "15"), ("16", "16"), ("16", "17"),
                                ("17", "8"), ("17", "16"), ("17", "17"), ("17", "18"),
                                ("18", "9"), ("18", "10"), ("18", "17"), ("18", "18"), ("18", "19"),
                                ("19", "10"), ("19", "18"), ("19", "19"), ("19", "20"),
                                ("20", "11"), ("20", "12"), ("20", "19"), ("20", "20"))

    val rddDataNearbyDistrict = spark.sparkContext.parallelize(dataNearbyDistrict)
    val dfDataNearbyDistrict = spark.createDataFrame(rddDataNearbyDistrict).toDF("district", "nearDistrict")

    dfDataNearbyDistrict.show

    val dfParisData = spark.read.format("csv").option("sep", ";").option("header", "true")
                          .load("src/main/resources/OutputData/stationnement.csv")

    val dfParisDataEssential = dfParisData.drop("parkName")
                                          .drop("address")
                                          .drop("type")
                                          .drop("maxHeight")
                                          .drop("schedule")
                                          .drop("surfaceLift")
                                          .drop("car")
                                          .drop("motorcycle")
                                          .drop("electricalCar")
                                          .drop("subscriptionMonth")
                                          .drop("price1Hour")
                                          .drop("parkPMR")
                                          .drop("subscriptionPMRMonth")
                                          .drop("subscriptionElectricalCarMonth")
                                          .drop("parkMotorcycle")
                                          .drop("price1HourMotorcycle")
                                          .drop("subscriptionMotorcycleMonth")
                                          .drop("particularSystem")
                                          .drop("prioritySystem")
                                          .drop("totalPark")
                                          .cache

    println(dfParisDataEssential.count + " Rows")
    dfParisDataEssential.show

    /**
     * SQL Area
     */

    dfDataNearbyDistrict.createOrReplaceTempView("NearbyDistrict")
    dfParisDataEssential.createOrReplaceTempView("ParisData")

    val result = spark.sql("SELECT ParisDataPrime.id, ParisDataPrime.district, ParisDataPrime.longitudeGeo, ParisDataPrime.latitudeGeo, ParisDataSecond.id AS joinId, ParisDataSecond.district AS joinDistrict, ParisDataSecond.longitudeGeo AS joinLongitudeGeo, ParisDataSecond.latitudeGeo AS joinLatitudeGeo FROM ParisData AS ParisDataPrime, ParisData AS ParisDataSecond, NearbyDistrict WHERE ParisDataPrime.district = NearbyDistrict.district AND NearbyDistrict.nearDistrict = ParisDataSecond.district ORDER BY ParisDataPrime.id, ParisDataSecond.id")

    //result.printSchema()
    //result.show()

    val interResult = result.filter(col("id") !== col("joinId"))
      .withColumn("haversine", getDistanceUDF(col("longitudeGeo"), col("latitudeGeo"), col("joinLongitudeGeo"), col("joinLatitudeGeo")))
      .orderBy(col("id"), col("haversine"))
      .persist(StorageLevel.DISK_ONLY)

    //println(interResult.count + "rows")
    //interResult.show
    //interResult.printSchema

    val w = Window.partitionBy(col("id")).orderBy(col("haversine"))

    val finalResult = interResult.withColumn("rn", row_number.over(w))
                                 .where(col("rn") <= nFirstRows)
                                 .drop("rn")
                                 .orderBy(col("id"), col("haversine"))

    //Create the output file, it writes one outputFile with the header and can ovewrite it
    finalResult.coalesce(1).write.option("header", "true").option("sep",";").mode("overwrite").csv("src/main/resources/OutputGraph")

  }

  //Create a distance function with Haversine
  val AVERAGE_RADIUS_OF_EARTH_M = 6371000

  def getDistance(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double): Int = {
    val startLatitudeRadian = Math.toRadians(sourceLat)
    val startLongitudeRadian = Math.toRadians(sourceLong)
    val finishLatitudeRadian = Math.toRadians(destLat)
    val finishLongitudeRadian = Math.toRadians(destLong)
    val tmpLatSinSquare = (1 - Math.cos(finishLatitudeRadian - startLatitudeRadian)) / 2
    val tmpLongSinSquare = (1 - Math.cos(finishLongitudeRadian - startLongitudeRadian)) / 2
    (2 * AVERAGE_RADIUS_OF_EARTH_M * Math.asin(Math.sqrt(tmpLatSinSquare + Math.cos(startLatitudeRadian) * Math.cos(finishLatitudeRadian) * tmpLongSinSquare))).round.toInt
  }

  //User-Defined Functions (aka UDF) is a feature of Spark SQL to define new Column-based functions
  def getDistanceUDF = udf[Int, Double, Double, Double, Double]{(value1: Double, value2: Double, value3: Double, value4: Double) =>
    val startLatitudeRadian = Math.toRadians(value1)
    val startLongitudeRadian = Math.toRadians(value2)
    val finishLatitudeRadian = Math.toRadians(value3)
    val finishLongitudeRadian = Math.toRadians(value4)
    val tmpLatSinSquare = (1 - Math.cos(finishLatitudeRadian - startLatitudeRadian)) / 2
    val tmpLongSinSquare = (1 - Math.cos(finishLongitudeRadian - startLongitudeRadian)) / 2
    (2 * AVERAGE_RADIUS_OF_EARTH_M * Math.asin(Math.sqrt(tmpLatSinSquare + Math.cos(startLatitudeRadian) * Math.cos(finishLatitudeRadian) * tmpLongSinSquare))).round.toInt
  }
}
