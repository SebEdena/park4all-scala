package MainGraph

import java.io.File
import java.nio.file.{Files, StandardCopyOption}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{concat, lit, _}

object ParisData {

  def generateData(spark: SparkSession): Unit ={

    val stationnementConcede = spark.read.format("csv").option("sep", ";").option("header", "true")
    .load("src/main/resources/InputData/parcs-de-stationnement-concedes-de-la-ville-de-paris.csv")

    val stationnementPublique = spark.read.format("csv").option("sep", ";").option("header", "true")
    .load("src/main/resources/InputData/stationnement-voie-publique-emplacements.csv")

    val stationnementConcedeJoin = spark.read.format("csv").option("sep", ";").option("header", "true")
    .load("src/main/resources/InputData/totalPlaces.csv")

    //.option("inferSchema", "true") means Spark guess what is the type dof the column => YES or NO
    //less performances to infer the schema, the file is read twice (once for the schema infer and once for the dataset)

    println("stationnementConcede = " + stationnementConcede.count + ", stationnementVoiePubliqueEmplacement = " + stationnementPublique.count) // stationnementConcede = 173, stationnementVoiePubliqueEmplacement = 59937

    //Remove all the colomns which are not interesting for the project
    val stationnementConcedeRemoved = stationnementConcede.drop("ID", "ADRESS_SSC", "ACCES_VL", "SITE_DELEG", "TEL", "TYPE_PARC", "ACCES_VELO", "AUTOPART",
                                                                "DATE_TA", "AB_1A_E", "TARIF_PR", "PR_1A_MINE", "PR_1A_MAXE", "TARIF_RES", "TF_RES_1AE", "DATE_TH",
                                                                "TF_15MN_E", "TF_30MN_E", "TF_1H30_E", "TF_2H_E", "TF_3H_E", "TF_4H_E", "TF_7H_E", "TF_8H_E", "TF_9H_E",
                                                                "TF_10H_E", "TF_11H_E", "TF_12H_E", "TF_24H_E", "ABPMR_1T_E","ABPMR_1A_E", "ABVE_1T_E", "ABVE_1A_E",
                                                                "ABMOTO_1TE", "ABMOTO_1AE", "TVELO_1M_E", "ZONES_RES", "TF_15MN_MO", "TF_30MN_MO", "TF_24H_MO",
                                                                "TF_PR_MOTO", "PR_1A_MINM", "PR_1A_MAXM", "TF_RES_MO", "TF_RES_1AM", "AB_1A_PATT", "PARC_AMOD",
                                                                "PARC_RELAI", "AB_1M_RELA", "MIS_A_JOUR", "TF_24H_MOT", "POINT_X", "POINT_Y", "geo_shape")

    val stationnemenPubliqueRemoved = stationnementPublique.drop("Type de stationnement", "Longueur", "Largeur", "ID", "ID_OLD", "PLACAL", "DATERELEVE",
                                                                "LOCSTA", "COMPNUMVOIE", "LOCNUM", "TYPEMOB", "NUMMOB","SIGNHOR", "SIGNVERT", "CONFSIGN", "NUMSSVP",
                                                                " NUMSSVP", "PARITE", "PLAGE_HOR1_DEBUT", "PLAGE_HOR1_FIN", "PLAGE_HOR2_DEBUT", "PLAGE_HOR2_FIN",
                                                                "PLAGE_HOR3_DEBUT", "PLAGE_HOR3_FIN", "LONGUEUR_CALCULEE", "SURFACE_CALCULEE", "MTCREATION_DATE_FIELD",
                                                                "MTLAST_EDIT_DATE_FIELD", "SOCIETE", "SURFACE_OLD", "LONGUEUR_OLD", "C_VOIE_VP", "N_SQ_TV", "NUMILOT",
                                                                "NUMIRIS", "ZONERES", "ZONEASP", "STV", "PREFET", "geo_shape")

    //stationnementConcedeRemoved.printSchema
    //stationnemenPubliqueRemoved.printSchema

    //stationnementConcedeRemoved.show
    //stationnemenPubliqueRemoved.show

    //Renaming columns
    val stationnementConcedeRenamed = stationnementConcedeRemoved.withColumnRenamed("OBJECTID","id")
                                                                .withColumnRenamed("NOM_PARC","parkName")
                                                                .withColumnRenamed("ADRESS_GEO","address")
                                                                .withColumnRenamed("Arrdt","district")
                                                                .withColumnRenamed("DELEG","type")
                                                                .withColumnRenamed("ASC_SURF", "surfaceLift")
                                                                .withColumnRenamed("H_PARC_CM","maxHeight")
                                                                .withColumnRenamed("H2_PARC_CM","maxHeight_2")
                                                                .withColumnRenamed("HORAIRE_NA","schedule")
                                                                .withColumnRenamed("ACCES_MOTO","motorcycle")
                                                                .withColumnRenamed("V_ELEC_CH","electricalCar")
                                                                .withColumnRenamed("AB_1M_E","subscriptionMonth")
                                                                .withColumnRenamed("TF_1H_E","price1Hour")
                                                                .withColumnRenamed("NB_PL_PMR","parkPMR")
                                                                .withColumnRenamed("ABPMR_1M_E","subscriptionPMRMonth")
                                                                .withColumnRenamed("ABVE_1M_E","subscriptionElectricalCarMonth")
                                                                .withColumnRenamed("NB_PL_MOTO","parkMotorcycle")
                                                                .withColumnRenamed("TMOTO_1EHE","price1HourMotorcycle")
                                                                .withColumnRenamed("ABMOTO_1ME","subscriptionMotorcycleMonth")
                                                                .withColumnRenamed("geo_point_2d","geo")

    val stationnemenPubliqueRenamed = stationnemenPubliqueRemoved.withColumnRenamed("OBJECTID","id")
                                                                .withColumnRenamed("Régime prioritaire","prioritySystem")
                                                                .withColumnRenamed("Régime particulier","particularSystem")
                                                                .withColumnRenamed("Arrondissement","district")
                                                                .withColumnRenamed("Nom de voie","wayName")
                                                                .withColumnRenamed("PLAREL","totalPark")
                                                                .withColumnRenamed("NUMVOIE","wayNumber")
                                                                .withColumnRenamed("TYPEVOIE","wayType")
                                                                .withColumnRenamed("TAR","price1Hour")
                                                                .withColumnRenamed("geo_point_2d","geo")

    //stationnementConcedeRenamed.printSchema
    //stationnemenPubliqueRenamed.printSchema

    //stationnementConcedeRenamed.show
    //stationnemenPubliqueRenamed.show

    //Cleaning Data
    //Stationnement concede
    val stationnementConcedeClean = stationnementConcedeRenamed.withColumn("id", concat(lit("c"), col("id")))
                                                              .withColumn("district", when(col("district") === "21", "12").when(col("district") === "22", "16").otherwise(col("district")))
                                                              .withColumn("surfaceLift", when(col("surfaceLift") === "ELEVATEUR PREVU" || col("surfaceLift") === "OUI" || col("surfaceLift") === "PREVU" || col("surfaceLift") === "ACCESSIBLE DE PLAIN PIED", "true").otherwise("false"))
                                                              .withColumn("motorcycle", when(col("motorcycle") === "OUI", "true").otherwise("false"))
                                                              .withColumn("electricalCar", when(col("electricalCar") === "OUI", "true").otherwise("false"))
                                                              .withColumn("subscriptionMonth", when(col("subscriptionMonth") === "ND", "null").otherwise(col("subscriptionMonth")))
                                                              .withColumn("subscriptionMonth", regexp_replace(col("subscriptionMonth"), ",", "."))
                                                              .withColumn("price1Hour", when(col("price1Hour") === "ND", "null").otherwise(col("price1Hour")))
                                                              .withColumn("price1Hour", regexp_replace(col("price1Hour"), ",", "."))
                                                              .withColumn("price1Hour", when(col("price1Hour") === "Gratuit", "0").otherwise(col("price1Hour")))
                                                              .withColumn("subscriptionPMRMonth", when(col("subscriptionPMRMonth") === "ND", "null").otherwise(col("subscriptionPMRMonth")))
                                                              .withColumn("subscriptionPMRMonth", regexp_replace(col("subscriptionPMRMonth"), ",", "."))
                                                              .withColumn("subscriptionElectricalCarMonth", when(col("subscriptionElectricalCarMonth") === "ND", "null").otherwise(col("subscriptionElectricalCarMonth")))
                                                              .withColumn("subscriptionElectricalCarMonth", regexp_replace(col("subscriptionElectricalCarMonth"), ",", "."))
                                                              .withColumn("price1HourMotorcycle", when(col("price1HourMotorcycle") === "ND", "null").otherwise(col("price1HourMotorcycle")))
                                                              .withColumn("price1HourMotorcycle", regexp_replace(col("price1HourMotorcycle"), ",", "."))
                                                              .withColumn("subscriptionMotorcycleMonth", when(col("subscriptionMotorcycleMonth") === "ND", "null").otherwise(col("subscriptionMotorcycleMonth")))
                                                              .withColumn("subscriptionMotorcycleMonth", regexp_replace(col("subscriptionMotorcycleMonth"), ",", "."))
                                                              .withColumn("maxHeight", when(col("maxHeight_2") > col("maxHeight"), col("maxHeight_2")).otherwise(col("maxHeight")))
                                                              .drop("maxHeight_2")
                                                              .withColumn("geo", split(col("geo"), ","))
                                                              .select( col("id"), col("parkName"), col("address"), col("district"), col("type"), col("maxHeight"), col("schedule"), col("surfaceLift"), col("motorcycle"), col("electricalCar"), col("subscriptionMonth"), col("price1Hour"), col("parkPMR"), col("subscriptionPMRMonth"), col("subscriptionElectricalCarMonth"), col("parkMotorcycle"), col("price1HourMotorcycle"), col("subscriptionMotorcycleMonth"), col("geo").getItem(0).as("latitudeGeo"), col("geo").getItem(1).as("longitudeGeo"))

    //Stationnement publique
    val stationnemenPubliqueClean = stationnemenPubliqueRenamed.withColumn("id", concat(lit("p"), col("id")))
                                                              .withColumn("district", when(col("district") === "21", "12").when(col("district") === "22", "16").otherwise(col("district")))
                                                              .na.fill("",Seq("wayNumber"))
                                                              .withColumn("address", concat(col("wayNumber"), lit(" "), col("wayType"), lit(" "), col("wayName")))
                                                              .drop("wayName")
                                                              .drop("wayType")
                                                              .drop("wayNumber")
                                                              .withColumn("price1Hour", regexp_replace(col("price1Hour"), ",", "."))
                                                              .withColumn("price1Hour", split(col("price1Hour"), "€/h"))
                                                              .select( col("id"), col("address"), col("prioritySystem"), col("particularSystem"), col("district"), col("totalPark"), col("price1Hour").getItem(0).as("price1Hour"), col("geo"))
                                                              .withColumn("type", concat(lit("public")))
                                                              .filter(not(col("particularSystem") === "Vélos" || col("particularSystem") === "Travaux" || col("particularSystem") === "Vélib'" || col("particularSystem") === "Transport de fonds"
                                                              || col("particularSystem") === "Taxi gaine autorisée" || col("particularSystem") === "SP - Police" || col("particularSystem") === "cd-cmd"
                                                              || col("particularSystem") === "Véhicules électriques partagés"|| col("particularSystem") === "Taxi gaine interdite" || col("particularSystem") === "véhicule partagé"
                                                              || col("particularSystem") === "Autocar (payant)" || col("particularSystem") === "Inaccessible" || col("particularSystem") === "SP - Véhicules Postaux"
                                                              || col("particularSystem") === "SP - Pompiers" || col("particularSystem") === "SP - Services de secours (SAMU, SMUR, protection civile)"
                                                              || col("particularSystem") === "SP - Nettoiement" || col("particularSystem") === "SP - Ministère" || col("particularSystem") === "SP - Mairie"
                                                              || col("particularSystem") === "Autocar dépose/reprise" || col("particularSystem") === "Autopartage" || col("particularSystem") === "SP - Administration"
                                                              || col("particularSystem") === "Autocar dépose" || col("particularSystem") === "Autocar reprise" || col("particularSystem") === "SP - Juridiction"
                                                              || col("particularSystem") === "Air France" || col("particularSystem") === "SP - Gendarmerie" || col("particularSystem") === "Trilib'"))
                                                              .withColumn("car", when(col("particularSystem") === "GIG/GIC Standard" || col("particularSystem") === "GIG/GIC Elargie" || col("particularSystem") === "Ex Autolib' 2 avec bornes de recharge"
                                                              || col("particularSystem") === "Ex Autolib' 1 avec bornes de recharge" || col("particularSystem") === "Bélib'" || col("particularSystem") === "Motos", "false").otherwise("true"))
                                                              .withColumn("parkPMR", when(col("particularSystem") === "GIG/GIC Standard" || col("particularSystem") === "GIG/GIC Elargie", 1).otherwise("null"))
                                                              .withColumn("electricalCar", when(col("particularSystem") === "Ex Autolib' 2 avec bornes de recharge" || col("particularSystem") === "Ex Autolib' 1 avec bornes de recharge", "true").otherwise("false"))
                                                              .filter(not(col("address") === "\"\""))
                                                              .withColumn("geo", split(col("geo"), ","))
                                                              .select( col("id"), col("address"), col("prioritySystem"), col("particularSystem"), col("district"), col("totalPark"), col("price1Hour"), col("type"), col("car"), col("parkPMR"), col("electricalCar"), col("geo").getItem(0).as("latitudeGeo"), col("geo").getItem(1).as("longitudeGeo"))
                                                              .withColumn("price1Hour", when(col("price1Hour") === "Gratuit", "0").otherwise(col("price1Hour")))

    /** particularSystem column
     *
     * Delete : "travaux", "Vélib", "Transport de fonds", "Taxi gaine autorisée", "SP - Police", "cd-cmd", "Véhicules électriques partagés", "Taxi gaine interdite", "véhicule partagé", "Autocar (payant)",
     *          "Inaccessible", "SP - Véhicules Postaux", "SP - Pompiers", "SP - Services de secours (SAMU, SMUR, protection civile)", "SP - Nettoiement", "SP - Ministère", "SP - Mairie", "Autocar dépose/reprise",
     *          "Autopartage", "SP - Administration", "Autocar dépose", "Autocar reprise", "SP - Juridiction", "Air France", "SP - Gendarmerie" "Trilib'"
     *
     * parkPMR : "GIG/GIC Standard", "GIG/GIC Elargie"
     *
     * motos : "Motos"
     *
     * electrique : "Ex Autolib' 2 avec bornes de recharge", "Ex Autolib' 1 avec bornes de recharge" "Bélib'"
     *
     */

    //stationnementConcedeClean.printSchema
    //stationnemenPubliqueClean.printSchema

    //stationnementConcedeClean.show
    //stationnemenPubliqueClean.show

    //Join "Stationnement condeces"
    val stationnementConcedeCleanJoin = stationnementConcedeClean.join(stationnementConcedeJoin, Seq("id"), "fullouter")
                                                                .filter(not(col("totalPlaces") === "null"))
                                                                .withColumnRenamed("totalPlaces","totalPark")

    //stationnementConcedeCleanJoin.printSchema
    //stationnementConcedeCleanJoin.show

    //Union
    val stationnementConcedeUnion = stationnementConcedeCleanJoin.withColumn("particularSystem", concat(lit("null")))
                                                                .withColumn("prioritySystem", concat(lit("null")))
                                                                .withColumn("car", concat(lit("true")))
                                                                .select( col("id"), col("parkName"), col("address"), col("district"),
                                                                col("type"), col("maxHeight"), col("schedule"), col("surfaceLift"),
                                                                col("car"),col("motorcycle"), col("electricalCar"), col("subscriptionMonth"),
                                                                col("price1Hour"), col("parkPMR"), col("subscriptionPMRMonth"),
                                                                col("subscriptionElectricalCarMonth"), col("parkMotorcycle"), col("price1HourMotorcycle"),
                                                                col("subscriptionMotorcycleMonth"), col("longitudeGeo"), col("latitudeGeo"),
                                                                col("particularSystem"), col("prioritySystem"), col("totalPark"))

    val stationnementPubliqueUnion = stationnemenPubliqueClean.withColumn("parkName", concat(col("address")))
                                                              .withColumn("maxHeight", concat(lit("null")))
                                                              .withColumn("schedule", concat(lit("null")))
                                                              .withColumn("surfaceLift", concat(lit("null")))
                                                              .withColumn("motorcycle", concat(lit("true")))
                                                              .withColumn("subscriptionMonth", concat(lit("null")))
                                                              .withColumn("subscriptionPMRMonth", concat(lit("null")))
                                                              .withColumn("subscriptionElectricalCarMonth", concat(lit("null")))
                                                              .withColumn("parkMotorcycle", concat(lit("null")))
                                                              .withColumn("price1HourMotorcycle", concat(lit("null")))
                                                              .withColumn("subscriptionMotorcycleMonth", concat(lit("null")))
                                                              .select( col("id"), col("parkName"), col("address"),
                                                              col("district"), col("type"), col("maxHeight"), col("schedule"),
                                                              col("surfaceLift"), col("car"), col("motorcycle"), col("electricalCar"),
                                                              col("subscriptionMonth"), col("price1Hour"), col("parkPMR"),
                                                              col("subscriptionPMRMonth"), col("subscriptionElectricalCarMonth"), col("parkMotorcycle"),
                                                              col("price1HourMotorcycle"), col("subscriptionMotorcycleMonth"), col("longitudeGeo"),
                                                              col("latitudeGeo"), col("particularSystem"), col("prioritySystem"), col("totalPark"))

    //stationnementConcedeUnion.printSchema
    //stationnementPubliqueUnion.printSchema

    stationnementConcedeUnion.show
    stationnementPubliqueUnion.show

    val stationnementUnion = stationnementConcedeUnion.union(stationnementPubliqueUnion)
    println("stationnementUnion = " + stationnementUnion.count) //beginning 60110, now 49687, 45468, 45380
    stationnementUnion.show

    //stationnemenPubliqueClean.select(stationnemenPubliqueClean("particularSystem")).distinct.show
    //stationnemenPubliqueClean.select(stationnemenPubliqueClean("prioritySystem")).distinct.show

    //stationnementConcedeUnion.select(stationnementConcedeUnion("totalPark")).distinct.collect.foreach(println)

    //stationnementUnion.select(stationnementUnion("schedule")).distinct.show
    //stationnementUnion.select(stationnementUnion("schedule")).distinct.show

    /**
     * SQL AREA
     */

    stationnementUnion.createOrReplaceTempView("stationnement")
    //spark.sql("SELECT * FROM stationnement WHERE district = '20' ").show() //stationnementUnion.filter(col("id") === "p48248" ).show()
    //spark.sql("SELECT * FROM stationnement WHERE address = '' ").show() //stationnementUnion.filter(col("id") === "p48248" ).show()
    //spark.sql("SELECT DISTINCT prioritySystem,particularSystem FROM stationnement ORDER BY prioritySystem").show(30,false) //stationnementUnion.filter(col("id") === "p48248" ).show()


    //Create the output file, it writes one outputFile with the header and can ovewrite it
    stationnementUnion.coalesce(1).write.option("header", "true").option("sep",";").mode("overwrite").csv("src/main/resources/OutputSpark")
  }

  def storeDataFile(): Unit = {
    def getListOfFiles(dir: String): List[String] = {
      val file = new File(dir)
      file.listFiles.filter(_.isFile)
        .map(_.getName)
        .toList
    }

    val pathCopyFile = "src/main/resources/OutputSpark/"
    val pathPasteFile = "src/main/resources/OutputData/"
    val newNameFile = "stationnement.csv"

    //Rename and store the final file
    val dir = new File(pathPasteFile)
    dir.mkdir

    val outputFiles = getListOfFiles(pathCopyFile)
    val selectDataFile = outputFiles.filter(x => x.endsWith(".csv"))

    val source = new File(pathCopyFile + selectDataFile.head)
    val dest = new File(pathPasteFile + newNameFile)

    Files.copy(source.toPath, dest.toPath, StandardCopyOption.REPLACE_EXISTING)
  }

}

/** HTTP Get
 *
 * val response: HttpResponse[String] = Http("http://www.shigemk2.com").asString
 * println("Body = " + response.body)
 * println("Code = " + response.code)
 * println("Headers = " + response.headers)
 * println("Cookies = " + response.cookies)
 * println(response.isError)
 * println(getListOfFiles("src/main/resources"))
 */