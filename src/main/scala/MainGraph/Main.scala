package MainGraph

import org.apache.spark.sql.SparkSession

object Main extends App{

  //Start the spark session
  val spark: SparkSession = SparkSession.builder.appName("Park4all").config("spark.master", "local[*]").getOrCreate
  spark.sparkContext.setLogLevel("WARN")
  spark.conf.set("spark.network.timeout", "600s") //Avoid RpcTimeoutException
  spark.conf.set("spark.driver.memory", "3g") //Add memory on the driver
  spark.conf.set("spark.executor.memory", "3g") //Add memory on the executor

  //Generate and store all the data
  ParisData.generateData(spark)
  ParisData.storeDataFile()

  //Generate and store the graph (Cluster Version)
  //ParisGraph.joinDistrict(spark, 5)
  //ParisGraph.generateGraph(spark)

  //Generate and store the graph (Laptop Version)
  //ParisGraphLaptop.joinDistrict(spark, 5)
  //ParisGraphLaptop.storeDataFile

  //Stop the spark session
  spark.stop
}