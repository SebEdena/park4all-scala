package MainGraph

import java.io.File
import java.nio.file.{Files, StandardCopyOption}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import scala.collection.immutable.HashMap

object ParisGraphLaptop extends Serializable{

  def joinDistrict(spark: SparkSession, nFirstRows: Int): Unit= {
    val dataNearbyDistrict = Seq(("1", "1"), ("1", "2"), ("1", "3"), ("1", "4"), ("1", "6"), ("1", "7"), ("1", "8"), ("1", "9"),
                                ("2", "1"), ("2", "2"), ("2", "3"), ("2", "9"), ("2", "10"),
                                ("3", "1"), ("3", "2"), ("3", "3"), ("3", "4"), ("3", "10"), ("3", "11"),
                                ("4", "1"), ("4", "3"), ("4", "4"), ("4", "5"), ("4", "11"), ("4", "12"),
                                ("5", "4"), ("5", "5"), ("5", "6"), ("5", "12"), ("5", "13"), ("5", "14"),
                                ("6", "1"), ("6", "5"), ("6", "6"), ("6", "7"), ("6", "14"), ("6", "15"),
                                ("7", "1"), ("7", "6"), ("7", "7"), ("7", "8"), ("7", "15"), ("7", "16"),
                                ("8", "1"), ("8", "7"), ("8", "8"), ("8", "9"), ("8", "16"), ("8", "17"),
                                ("9", "1"), ("9", "2"), ("9", "8"), ("9", "9"), ("9", "10"), ("9", "18"),
                                ("10", "2"), ("10", "3"), ("10", "9"), ("10", "10"), ("10", "11"), ("10", "18"), ("10", "19"),
                                ("11", "3"), ("11", "4"), ("11", "10"), ("11", "11"), ("11", "12"), ("11", "20"),
                                ("12", "4"), ("12", "5"), ("12", "11"), ("12", "12"), ("12", "13"), ("12", "20"),
                                ("13", "5"), ("13", "12"), ("13", "13"), ("13", "14"),
                                ("14", "5"), ("14", "6"), ("14", "13"), ("14", "14"), ("14", "15"),
                                ("15", "6"), ("15", "7"), ("15", "14"), ("15", "15"), ("15", "16"),
                                ("16", "7"), ("16", "8"), ("16", "15"), ("16", "16"), ("16", "17"),
                                ("17", "8"), ("17", "16"), ("17", "17"), ("17", "18"),
                                ("18", "9"), ("18", "10"), ("18", "17"), ("18", "18"), ("18", "19"),
                                ("19", "10"), ("19", "18"), ("19", "19"), ("19", "20"),
                                ("20", "11"), ("20", "12"), ("20", "19"), ("20", "20"))

    val dataNearbyDistrictHashMap = HashMap(1->List("1", "2", "3", "4", "6", "7", "8", "9"),
                                            2->List("1", "2", "3", "9", "10"),
                                            3->List("1", "2", "3", "4", "10", "11"),
                                            4->List("1", "3", "4", "5", "11", "12"),
                                            5->List("4", "5", "6", "12", "13", "14"),
                                            6->List("1", "5", "6", "7", "14", "15"),
                                            7->List("1", "6", "7", "8", "15", "16"),
                                            8->List("1", "7", "8", "9", "16", "17"),
                                            9->List("1", "2", "8", "9", "10", "18"),
                                            10->List("2", "3", "9", "10", "11", "18", "19"),
                                            11->List("3", "4", "10", "11", "12", "20"),
                                            12->List("4", "5", "11", "12", "13", "20"),
                                            13->List("5", "12", "13", "14"),
                                            14->List("5", "6", "13", "14", "15"),
                                            15->List("6", "7", "14", "15", "16"),
                                            16->List("7", "8", "15", "16", "17"),
                                            17->List("8", "16", "17", "18"),
                                            18->List("9", "10", "17", "18", "19"),
                                            19->List("10", "18", "19", "20", "12", "20"),
                                            20->List("11", "12", "19", "20"))

    val rddDataNearbyDistrict = spark.sparkContext.parallelize(dataNearbyDistrict)
    val dfDataNearbyDistrict = spark.createDataFrame(rddDataNearbyDistrict).toDF("district", "nearDistrict")

    val dfParisData = spark.read.format("csv").option("sep", ";").option("header", "true").load("src/main/resources/OutputData/stationnement.csv")

    val dfParisDataEssential = dfParisData.drop("parkName")
                                          .drop("address")
                                          .drop("type")
                                          .drop("maxHeight")
                                          .drop("schedule")
                                          .drop("surfaceLift")
                                          .drop("car")
                                          .drop("motorcycle")
                                          .drop("electricalCar")
                                          .drop("subscriptionMonth")
                                          .drop("price1Hour")
                                          .drop("parkPMR")
                                          .drop("subscriptionPMRMonth")
                                          .drop("subscriptionElectricalCarMonth")
                                          .drop("parkMotorcycle")
                                          .drop("price1HourMotorcycle")
                                          .drop("subscriptionMotorcycleMonth")
                                          .drop("particularSystem")
                                          .drop("prioritySystem")
                                          .drop("totalPark")
                                          .cache

    dfDataNearbyDistrict.createOrReplaceTempView("NearbyDistrict")

    //Generate the edges for each district one per one because of the performance issue
    outputGraphDistrictN(spark, 1, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 2, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 3, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 4, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 5, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 6, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 7, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 8, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 9, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 10, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 11, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 12, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 13, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 14, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 15, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 16, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 17, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 18, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 19, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)
    outputGraphDistrictN(spark, 20, dfParisDataEssential, dataNearbyDistrictHashMap, nFirstRows)

    //Read all the csv files of each district to make a union
    val dataDistrict1 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(1))
    val dataDistrict2 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(2))
    val dataDistrict3 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(3))
    val dataDistrict4 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(4))
    val dataDistrict5 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(5))
    val dataDistrict6 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(6))
    val dataDistrict7 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(7))
    val dataDistrict8 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(8))
    val dataDistrict9 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(9))
    val dataDistrict10 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(10))
    val dataDistrict11 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(11))
    val dataDistrict12 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(12))
    val dataDistrict13 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(13))
    val dataDistrict14 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(14))
    val dataDistrict15= spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(15))
    val dataDistrict16 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(16))
    val dataDistrict17 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(17))
    val dataDistrict18 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(18))
    val dataDistrict19 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(19))
    val dataDistrict20 = spark.read.format("csv").option("sep", ";").option("header", "true").load(getNameDataFile(20))

    //union of all the districts
    val totalDataDistrict = dataDistrict1.union(dataDistrict2)
                                        .union(dataDistrict3)
                                        .union(dataDistrict4)
                                        .union(dataDistrict5)
                                        .union(dataDistrict6)
                                        .union(dataDistrict7)
                                        .union(dataDistrict8)
                                        .union(dataDistrict9)
                                        .union(dataDistrict10)
                                        .union(dataDistrict11)
                                        .union(dataDistrict12)
                                        .union(dataDistrict13)
                                        .union(dataDistrict14)
                                        .union(dataDistrict15)
                                        .union(dataDistrict16)
                                        .union(dataDistrict17)
                                        .union(dataDistrict18)
                                        .union(dataDistrict19)
                                        .union(dataDistrict20)

    println(totalDataDistrict.count)
    totalDataDistrict.coalesce(1).write.option("header", "true").option("sep",";").mode("overwrite").csv("src/main/resources/OutputGraph/AllDistrict")
  }

  def dfFilterDistrict(numDistrict: Int, sourceDataFrame: DataFrame, sourceHashMap: HashMap[Int, List[String]]): DataFrame = {
    val dataDistrictNumDistrict = sourceDataFrame.filter(col("district") === numDistrict)
    val dataDistrict = sourceDataFrame.filter(col("district").isin(sourceHashMap(numDistrict):_*))

    println("District " + numDistrict + " = " + dataDistrictNumDistrict.count + "/" + dataDistrict.count + " Rows")
    dataDistrict
  }

  def dfJoinDistrict(spark: SparkSession, numDistrict: Int, sourceDataFrame : DataFrame): DataFrame ={
    val nameTable = "ParisData" + numDistrict
    sourceDataFrame.createOrReplaceTempView(nameTable)

    val querySQL = "SELECT ParisDataPrime.id, ParisDataPrime.district, ParisDataPrime.longitudeGeo, ParisDataPrime.latitudeGeo, ParisDataSecond.id AS joinId, ParisDataSecond.district AS joinDistrict, ParisDataSecond.longitudeGeo AS joinLongitudeGeo, ParisDataSecond.latitudeGeo AS joinLatitudeGeo FROM " + nameTable + " AS ParisDataPrime, " + nameTable + " AS ParisDataSecond, NearbyDistrict WHERE ParisDataPrime.district = NearbyDistrict.district AND NearbyDistrict.nearDistrict = ParisDataSecond.district ORDER BY ParisDataPrime.id, ParisDataSecond.id"
    val result = spark.sql(querySQL)
                      .filter(col("district") === numDistrict)

    val interResult = result.filter(col("id") !== col("joinId"))
      .withColumn("haversine", getDistanceUDF(col("longitudeGeo"), col("latitudeGeo"), col("joinLongitudeGeo"), col("joinLatitudeGeo")))
      .filter(col("haversine").lt(500)) //for district 14, 15, 16, 17
      .orderBy(col("id"), col("haversine"))

    println("District " + numDistrict + " = "  + interResult.count + " Rows")
    interResult
  }

  def dfOnlyNRowPerId(numDistrict: Int, nRow: Int, sourceDataFrame: DataFrame): DataFrame = {
    val w = Window.partitionBy(col("id")).orderBy(col("haversine"))

    val finalResult = sourceDataFrame.withColumn("rn", row_number.over(w))
      .where(col("rn") <= nRow)
      .drop("rn")
      .orderBy(col("id"), col("haversine"))

    println("District " + numDistrict + " = "  + finalResult.count + " Rows")
    finalResult
  }

  def outputGraphDistrictN(spark: SparkSession, numDistrict: Int, sourceDataFrame: DataFrame, sourceHashMap: HashMap[Int, List[String]], nRow: Int): Unit = {
    val nameFolder = "District" + numDistrict
    println("Working on the " + nameFolder)
    val dataDistrict = dfFilterDistrict(numDistrict, sourceDataFrame, sourceHashMap)
    val dataDistrictJoin = dfJoinDistrict(spark, numDistrict, dataDistrict)
    val dataResult = dfOnlyNRowPerId(numDistrict, nRow, dataDistrictJoin)

    dataResult.coalesce(1).write.option("header", "true").option("sep",";").mode("overwrite").csv("src/main/resources/OutputGraph/" + nameFolder)
  }

  //Create a distance function with Haversine
  //User-Defined Functions (aka UDF) is a feature of Spark SQL to define new Column-based functions
  val AVERAGE_RADIUS_OF_EARTH_M = 6371000

  def getDistanceUDF = udf[Int, Double, Double, Double, Double]{(value1: Double, value2: Double, value3: Double, value4: Double) =>
    val startLatitudeRadian = Math.toRadians(value1)
    val startLongitudeRadian = Math.toRadians(value2)
    val finishLatitudeRadian = Math.toRadians(value3)
    val finishLongitudeRadian = Math.toRadians(value4)
    val tmpLatSinSquare = (1 - Math.cos(finishLatitudeRadian - startLatitudeRadian)) / 2
    val tmpLongSinSquare = (1 - Math.cos(finishLongitudeRadian - startLongitudeRadian)) / 2
    (2 * AVERAGE_RADIUS_OF_EARTH_M * Math.asin(Math.sqrt(tmpLatSinSquare + Math.cos(startLatitudeRadian) * Math.cos(finishLatitudeRadian) * tmpLongSinSquare))).round.toInt
  }

  def getListOfFiles(dir: String): List[String] = {
    val file = new File(dir)
    file.listFiles.filter(_.isFile)
      .map(_.getName)
      .toList
  }

  def getNameDataFile(numDistrict: Int): String = {
    val nameReadfile = "District" + numDistrict
    val pathReadFile = "src/main/resources/OutputGraph/" + nameReadfile

    val outputFiles = getListOfFiles(pathReadFile)
    val selectDataFile = outputFiles.filter(x => x.endsWith(".csv"))
    pathReadFile + "/" + selectDataFile.head
  }

  def storeDataFile(): Unit = {
    val pathCopyFile = "src/main/resources/OutputGraph/AllDistrict/"
    val pathPasteFile = "src/main/resources/OutputGraph/"
    val newNameFile = "graph.csv"

    //Rename and store the final file
    val dir = new File(pathPasteFile)
    dir.mkdir

    val outputFiles = getListOfFiles(pathCopyFile)
    val selectDataFile = outputFiles.filter(x => x.endsWith(".csv"))

    val source = new File(pathCopyFile + selectDataFile.head)
    val dest = new File(pathPasteFile + newNameFile)

    Files.copy(source.toPath, dest.toPath, StandardCopyOption.REPLACE_EXISTING)
  }

}
