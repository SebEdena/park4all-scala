package SparkTestingBaseXebia

case class Balance(id: Int,
                   income: Int,
                   outcome: Int)
