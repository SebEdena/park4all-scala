package MainGraph

import SparkTestingBaseXebia.SparkSessionProvider
import org.scalatest.{FlatSpec, Matchers}

class ParisGraphTest extends FlatSpec with Matchers with SparkSessionProvider {

  /*
  "Simple test" should "do something" in {
    // Given
    val inputDF = List(
      Balance(id = 1, income = 3, outcome = 2),
      Balance(id = 2, income = 3, outcome = 4)
    ).toDF()

    // When
    val result = new onlyPositiveBalance(inputDF)

    // Then
    result.schema.map(_.name) should contain allOf ("income", "outcome")
    result.count() shouldBe 1
    result.map(row => row.getInt(row.fieldIndex("id"))).head shouldBe 1
  }

   */

  /*test("The haversine distance between the point start and dest should return 3024") {
    val start = (48.880834, 2.362338)
    val dest = (48.855821, 2.3785623)
    ParisGraph.getDistance(start._1, start._2, dest._1, dest._2) shouldBe 3024
  }*/

  /*test("haversine spark"){

    val input1 = sc.parallelize(List(1, 2, 3)).toDF
    assertDataFrameEquals(input1, input1) // equal
    input1 shouldEqual(input1)

    val input2 = sc.parallelize(List(4, 5, 6)).toDF
    intercept[org.scalatest.exceptions.TestFailedException] {
      assertDataFrameEquals(input1, input2) // not equal
    }
  }*/
/*
  test("simple test") {
    val sqlCtx = sqlContext
    import sqlCtx.implicits._

    case class Balance( income: Int = 0, outcome: Int = 0)

    val input1 = List(
      Balance(income = 1, outcome = 1),
      Balance(income = 2, outcome = 2),
      Balance(income = 3, outcome = 3)).toDF

    //val input2 = sc.parallelize(List[(Int, Double)]((1, 1.2), (2, 2.3), (3, 3.4))).toDF
    assertDataFrameApproximateEquals(input1, input1, 0.11) // equal

    intercept[org.scalatest.exceptions.TestFailedException] {
      assertDataFrameApproximateEquals(input1, input1, 0.05) // not equal
    }
  }
*/

/*
  test("Calculate haversine distance"){

    // Given
    case class InputPositionFromTo(longitudeGeo: String, latitudeGeo: String , joinLongitudeGeo: String, joinLatitudeGeo: String)
    case class OutputPositionFromTo(longitudeGeo: String, latitudeGeo: String , joinLongitudeGeo: String, joinLatitudeGeo: String, haversine: String)

    //val input = sc.parallelize(List("0", "0", "0", "0")).toDF

    //val output = sc.parallelize(List("0", "0", "0", "0", "0")).toDF

    /*val inputDF = List(
      InputPositionFromTo(longitudeGeo = "0", latitudeGeo = "0", joinLongitudeGeo = "0", joinLatitudeGeo = "0"),
      InputPositionFromTo(longitudeGeo = "48.880834", latitudeGeo = "2.362338", joinLongitudeGeo = "48.855821", joinLatitudeGeo = "2.3785623")
    ).toDF()
     */

    val inputDF = sc.parallelize(List[InputPositionFromTo](
      InputPositionFromTo(longitudeGeo = "0", latitudeGeo = "0", joinLongitudeGeo = "0", joinLatitudeGeo = "0"),
      InputPositionFromTo(longitudeGeo = "48.880834", latitudeGeo = "2.362338", joinLongitudeGeo = "48.855821", joinLatitudeGeo = "2.3785623")
    )).toDF()

    val outputDF = List(
      OutputPositionFromTo(longitudeGeo = "0", latitudeGeo = "0", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
      OutputPositionFromTo(longitudeGeo = "0", latitudeGeo = "0", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "3024")
    ).toDF()


    // When
    val result = input.withColumn("haversine", getDistanceUDF(col("longitudeGeo"), col("latitudeGeo"), col("joinLongitudeGeo"), col("joinLatitudeGeo")))

    // Then
    intercept[org.scalatest.exceptions.TestFailedException]{
      assertDataFrameEquals(result, outputDF)
    }
  }*/

}