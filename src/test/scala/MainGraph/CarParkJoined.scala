package MainGraph

case class CarParkJoined(id: String,
                         district: String,
                         longitudeGeo: String,
                         latitudeGeo: String,
                         joinId: String,
                         joinDistrict: String,
                         joinLongitudeGeo: String,
                         joinLatitudeGeo: String,
                         haversine: String)
