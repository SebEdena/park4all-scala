package MainGraph

case class CarPark(id: String,
                   district: String,
                   longitudeGeo: String,
                   latitudeGeo: String)
