package MainGraph

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.scalatest.{FunSuite, Matchers}
import scala.collection.immutable.HashMap

class ParisGraphLaptopTest extends FunSuite with Matchers with DataFrameSuiteBase {

  val dataNearbyDistrictHashMap = HashMap(1->List("1", "2", "3", "4", "6", "7", "8", "9"),
                                          2->List("1", "2", "3", "9", "10"),
                                          3->List("1", "2", "3", "4", "10", "11"),
                                          4->List("1", "3", "4", "5", "11", "12"),
                                          5->List("4", "5", "6", "12", "13", "14"),
                                          6->List("1", "5", "6", "7", "14", "15"),
                                          7->List("1", "6", "7", "8", "15", "16"),
                                          8->List("1", "7", "8", "9", "16", "17"),
                                          9->List("1", "2", "8", "9", "10", "18"),
                                          10->List("2", "3", "9", "10", "11", "18", "19"),
                                          11->List("3", "4", "10", "11", "12", "20"),
                                          12->List("4", "5", "11", "12", "13", "20"),
                                          13->List("5", "12", "13", "14"),
                                          14->List("5", "6", "13", "14", "15"),
                                          15->List("6", "7", "14", "15", "16"),
                                          16->List("7", "8", "15", "16", "17"),
                                          17->List("8", "16", "17", "18"),
                                          18->List("9", "10", "17", "18", "19"),
                                          19->List("10", "18", "19", "20", "12", "20"),
                                          20->List("11", "12", "19", "20"))

  test("dfFilterDistrict function : Car park in district 1 is near from district 1") {
    val sqlCtx = sqlContext
    import sqlCtx.implicits._

    val dfInput = List(CarPark(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0")).toDF

    val dfResult = ParisGraphLaptop.dfFilterDistrict(1, dfInput, dataNearbyDistrictHashMap)
    assertDataFrameEquals(dfResult, dfInput) // equal
    }

  test("dfFilterDistrict function : Car park in district 20 isn't near from district 1") {
    val sqlCtx = sqlContext
    import sqlCtx.implicits._

    val dfInput = List(CarPark(id = "c1", district ="20", longitudeGeo = "0", latitudeGeo = "0")).toDF

    val dfResult = ParisGraphLaptop.dfFilterDistrict(1, dfInput, dataNearbyDistrictHashMap)
    intercept[org.scalatest.exceptions.TestFailedException] {
      assertDataFrameEquals(dfResult, dfInput) // not equal
    }
  }

  /*
  test("dfJoinDistrict function : Joining car parks ") {
    val sqlCtx = sqlContext
    import sqlCtx.implicits._

    val dfInput = List(CarPark(id = "c1", district ="1", longitudeGeo = "1", latitudeGeo = "1"),
                       CarPark(id = "c2", district ="2", longitudeGeo = "2", latitudeGeo = "2")).toDF

    val dfOutput = List(CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c2", joinDistrict ="2", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                        CarParkJoined(id = "c2", district ="2", longitudeGeo = "0", latitudeGeo = "0", joinId = "c1", joinDistrict ="1", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0")).toDF

    val dfResult = ParisGraphLaptop.dfJoinDistrict(spark, 1, dfInput)

      assertDataFrameEquals(dfResult, dfOutput) //equal
  }
   */

  test("dfOnlyNRowPerId function : take n joined car parks per id ") {
    val sqlCtx = sqlContext
    import sqlCtx.implicits._

    val dfInput = List(CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c1", joinDistrict ="1", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                      CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c2", joinDistrict ="2", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                      CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c3", joinDistrict ="3", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                      CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c4", joinDistrict ="4", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                      CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c5", joinDistrict ="5", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                      CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c6", joinDistrict ="6", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                      CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c7", joinDistrict ="7", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0")).toDF

    val dfOutput = List(CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c1", joinDistrict ="1", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                        CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c2", joinDistrict ="2", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                        CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c3", joinDistrict ="3", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                        CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c4", joinDistrict ="4", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0"),
                        CarParkJoined(id = "c1", district ="1", longitudeGeo = "0", latitudeGeo = "0", joinId = "c5", joinDistrict ="5", joinLongitudeGeo = "0", joinLatitudeGeo = "0", haversine = "0")).toDF

    val dfResult = ParisGraphLaptop.dfOnlyNRowPerId(1, 5, dfInput)

    assertDataFrameEquals(dfResult, dfOutput) //equal
  }

}
