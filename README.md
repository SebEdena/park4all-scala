# park4all-scala

This Repository is the data part of the project "park4all". <br/>
The project is a special project created by us students. <br/>
It is the end-of-semester project of the "Advanced Algorithmic and programming" course in the software engineering training given at ISEP. <br/> 
It serves to process all the data of the car parks for the park4all project. <br/> 

## Prerequisites 

* Scala 2.12.8
* Scala Build Tool (SBT)
* ScalaTest 3.0.8
* Spark 2.4.1
* Spark Testing Base 0.12.0
* ScalaJ 2.4.2
* Files of Open Data Paris

## Installation

Create a folder InputData in the resources folder <br/>
Put the 3 files in the folder park4all-scala/src/main/resources/InputData <br/> 
To Generate the csv file with all the car parks and the edges of the graph use this commands. <br/>
 
```
git clone https://gitlab.com/SebEdena/park4all-scala.git
cd park4all-scala/
sbt run 
```

To find the car park file follow this path park4all-scala/src/main/resources/OutputData/stationnement.csv <br/>
To find the edges of the graph file follow this path park4all-scala/src/main/resources/OutputGraph/graph.csv
## Authors
* Laurent Yu → @lauyu10
* Pierre Verbe → @PierreVerbe 
* Sébastien Viguier → @SebEdena
* Mathieu Valentin → @hmatico